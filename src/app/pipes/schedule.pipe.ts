import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'appSchedulePipe'
})

export class SchedulePipe implements PipeTransform {
  transform(value: number) {
    const startTime = 8 * 3600;
    const result = startTime + value;
    return moment.utc(result * 1000).format('HH:mm');
  }
}
