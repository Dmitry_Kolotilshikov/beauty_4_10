import { NgModule } from '@angular/core';
import { SchedulePipe } from './schedule.pipe';
import { CommonModule } from '@angular/common';
import { ServiceNamePipe } from './service-name.pipe';

@NgModule({
  declarations: [
    SchedulePipe,
    ServiceNamePipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SchedulePipe,
    ServiceNamePipe
  ]
})

export class PipesModule { }
