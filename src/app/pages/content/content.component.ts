import { Master, ScheduleItem, Work, Option, Order } from './../../models/data';
import { DataService, LoginService } from '@services/core';
import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { animation } from './animation';
import { EventObject } from 'ngx-fullcalendar';
import * as moment from 'moment';
import { CdkDragDrop, transferArrayItem, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-content-page',
  templateUrl: 'content.component.html',
  styleUrls: ['content.component.scss'],
  animations: animation,
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class ContentPageComponent implements OnInit, OnDestroy {

  private routeSubscriber: Subscription;
  private workSubscriber: Subscription;
  private mastersSubscriber: Subscription;

  public masters: Master[] = [];
  public type = 'brows';

  public state = 'masters';
  public selectedMaster: Master;

  public options: any;
  public events: EventObject[];

  public schedule: ScheduleItem[] = [];
  private work: Work;
  public selectedSchedule: ScheduleItem;

  public selectedOptions: Option[] = [];
  public workOptions: Option[] = [];

  public selectedDate: string;

  constructor(private activatedRoute: ActivatedRoute,
    private dataService: DataService,
    private loginService: LoginService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.setCalendarOptions();
    this.routeSubscriber = this.activatedRoute.params.subscribe(params => {
      this.type = params.type;
      this.state = 'masters';
      this.schedule = [];

      this.workSubscriber = this.dataService.getWork(params.type).subscribe(data => {
        this.work = data;
      });
      this.mastersSubscriber = this.dataService.getMasters(params.type).subscribe(data => {
        this.masters = data;
        if (this.masters.length > 0) {
          this.cdr.detectChanges();
        }
      });
    });
  }

  public get isCanSelectOptions(): boolean {
    const selectedScheduleIndex = this.schedule.indexOf(this.selectedSchedule);
    const nextSchedule = this.schedule[selectedScheduleIndex + 1];
    if (nextSchedule) {
      return !nextSchedule.is_reserved;
    }
    return true;
  }

  public drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      transferArrayItem(event.previousContainer.data, event.container.data,
        event.previousIndex, event.currentIndex);
    } else {
      moveItemInArray(this.workOptions, event.previousIndex, event.currentIndex);
    }
  }

  private setCalendarOptions(): void {
    this.options = {
      editable: true,
      height: 500,
      locale: 'ru',
      firstDay: 1
    };

    this.events = [];
  }

  public get isMastersState(): boolean {
    return this.state === 'masters';
  }

  public get isCalendarState(): boolean {
    return this.state === 'calendar';
  }

  public get isOrdersState(): boolean {
    return this.state === 'orders';
  }

  public goBack() {
    switch (this.state) {
      case 'orders':
        this.state = 'calendar';
        this.schedule = [];
        this.selectedSchedule = null;
        break;
      case 'calendar':
        this.state = 'masters';
        break;
      default:
        break;
    }
  }

  public selectMaster(master: Master): void {
    this.selectedMaster = master;
    this.state = 'calendar';
  }

  public onDateSelect(data: { dateStr: string }): void {
    const dayNumber = moment(data.dateStr, 'YYYY-MM-DD').weekday();
    const currentDate = moment().startOf('day').unix();
    const unixSelectedDate = moment(data.dateStr, 'YYYY-MM-DD').unix();
    if (dayNumber !== 0 && dayNumber !== 6) {
      this.selectedDate = data.dateStr;
      this.state = 'orders';
      this.loadOptions();
      this.createSchedule();
    }
  }

  public selectTime(item: ScheduleItem): void {
    if (!item.is_reserved) {
      this.selectedSchedule = item;
    }
  }

  public getIsActive(item: ScheduleItem): boolean {
    return (this.selectedSchedule && this.selectedSchedule.time === item.time);
  }

  private loadOptions(): void {
    this.dataService.getOptions(this.work.id).then(result => {
      this.workOptions = result;
    });
  }

  private createSchedule(): void {
    this.schedule = [];
    let isBreakSet: boolean;
    this.dataService.getOrders(this.selectedMaster.id, moment(this.selectedDate, 'YYYY-MM-DD')
      .unix()).then(result => {
        /* const steps = Math.round(9 * 3600 / this.work.time);
        for (let i = 0; i <= steps; i++) {
          const stepTime = i === 0 ? 0 : this.schedule[i - 1].time + this.work.time;
          const scheduleItem: ScheduleItem = {
            is_reserved: false,
            time: stepTime
          };
          if (stepTime > 4 * 3600 && !isBreakSet) {
            scheduleItem.is_reserved = true;
            isBreakSet = true;
            scheduleItem.time = this.schedule[i - 1].time + 3600;
          }
          if (result.some(item => item.time === stepTime)) {
            scheduleItem.is_reserved = true;
          }
          this.schedule.push(scheduleItem);
        } */

        let lastTime = 0;
        let count = 0;
        while (lastTime < 9 * 3600) {
          count++;
          const order = result.find(ord => ord.time === lastTime);
          const scheduleItem: ScheduleItem = {
            is_reserved: false,
            time: lastTime
          };
          if (lastTime >= 4 * 3600 && !isBreakSet) {
            isBreakSet = true;
            scheduleItem.is_reserved = true;
            lastTime += 3600;
            this.schedule.push(scheduleItem);
            continue;
          }
          if (order) {
            scheduleItem.is_reserved = true;
            if (order.options && order.options.length > 0) {
              const optionsTime = order.options.reduce((prev, current) => {
                return prev + this.workOptions.find(opt => +opt.option_id === +current).time;
              }, 0);
              lastTime = lastTime + optionsTime + this.work.time;
            } else {
              lastTime += this.work.time;
            }
          } else {
            lastTime += this.work.time;
          }
          if (9 * 3600 + 1800 >= lastTime) {
            this.schedule.push(scheduleItem);
          }
          if (count > 50) {
            return;
          }
        }
        this.cdr.detectChanges();
      });
  }

  public get totalSumm(): number {
    const optionsSumm = this.selectedOptions.reduce((acc, current) => acc + current.price, 0);
    const result = optionsSumm + this.work.price;
    return result;
  }

  public get isHaveSelectedOptions(): boolean {
    return this.selectedOptions.length > 0;
  }

  public confirmOrder(): void {
    const order: Order = {
      date: moment(this.selectedDate, 'YYYY-MM-DD').unix(),
      time: this.selectedSchedule.time,
      work_id: this.work.id,
      master_id: this.selectedMaster.id,
      options: this.selectedOptions.map(op => op.option_id),
      client_id: Number(this.loginService.token)
    };
    this.dataService.sendOrder(order).then(isComplete => {
      if (isComplete) {
        alert('Заказ принят');
        this.state = 'masters';
      } else {
        alert('Произошла ошибка. Попробуйте еще раз');
        this.createSchedule();
      }
      this.selectedSchedule = null;
      this.selectedOptions = [];
    }).catch(_ => {
      alert('Произошла ошибка. Попробуйте еще раз');
      this.createSchedule();
    }).finally(() => this.cdr.detectChanges());
  }

  ngOnDestroy() {
    if (this.routeSubscriber) {
      this.routeSubscriber.unsubscribe();
    }
    if (this.workSubscriber) {
      this.workSubscriber.unsubscribe();
    }
    if (this.mastersSubscriber) {
      this.mastersSubscriber.unsubscribe();
    }
  }
}
