import { trigger, state, style, transition, animate } from '@angular/animations';

export const animation = [
    trigger('fade', [
        state('void', style({opacity: 0, transform: 'translateX(-100%)'})),
        state('*', style({opacity: 1, transform: 'translateX(0)'})),
        transition('void <=> *', animate(200))
    ])
];
