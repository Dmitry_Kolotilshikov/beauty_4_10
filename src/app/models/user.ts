export interface User {
  id?: number;
  email: string;
  password: string;
  phone?: string;
  name?: string;
}
