import { Work, Master, Option, Order } from './../models/data';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class DataService {

  private readonly url = 'http://localhost:3000/';

  constructor(private http: HttpClient) { }

  getWork(type: string): Observable<Work> {
    const params = new HttpParams().set('name', type);
    return this.http.get<Work>(this.url.concat('work'), { params });
  }

  getMasters(type: string): Observable<Master[]> {
    const params = new HttpParams().set('name', type);
    return this.http.get<Master[]>(this.url.concat('master'), { params });
  }

  getOptions(workId: number): Promise<Option[]> {
    const params = new HttpParams().set('id', workId.toString());
    return this.http.get<Option[]>(this.url.concat('options'), { params }).toPromise();
  }

  sendOrder(order: Order): Promise<boolean> {
    return this.http.post<boolean>(this.url.concat('orders'), order).toPromise();
  }

  getOrders(masterId: number, date: number): Promise<Order[]> {
    const params = new HttpParams()
      .set('master_id', masterId.toString())
      .set('date', date.toString());
    return this.http.get<Order[]>(this.url.concat('orders'), { params }).toPromise();
  }

  getOrdersStats(): Promise<Order[]> {
    return this.http.get<Order[]>(this.url.concat('stats-orders')).toPromise();
  }
}


