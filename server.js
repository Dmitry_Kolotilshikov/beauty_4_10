var express = require('express');
var app = express();

var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var fs = require('fs');

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var users = [];

function getUserList() {
  return new Promise((resolve, reject) => {
    fs.readFile('./server-data/users.json', 'utf-8', (err, data) => {
      if (!err) {
        resolve(data);
      } else {
        reject();
      }
    });
  });
}

function saveUsers() {
  fs.writeFile('./server-data/users.json', JSON.stringify(users), () => { });
}

function getWorks() {
  return new Promise((resolve, reject) => {
    fs.readFile('./server-data/works.json', 'utf-8', (err, data) => {
      if (!err) {
        resolve(data);
      } else {
        reject();
      }
    });
  });
}

function getMasters() {
  return new Promise((resolve, reject) => {
    fs.readFile('./server-data/masters.json', 'utf-8', (err, data) => {
      if (!err) {
        resolve(data);
      } else {
        reject();
      }
    });
  });
}

function getOptions(work_id) {
  return new Promise((resolve, reject) => {
    fs.readFile('./server-data/options.json', 'utf-8', (err, data) => {
      if (!err) {
        const allOptions = JSON.parse(data);
        const filteredOptions = allOptions.filter(opt => +opt.work_id === +work_id);
        resolve(filteredOptions);
      } else {
        reject();
      }
    });
  });
}

function saveOrder(order) {
  return new Promise((resolve, reject) => {
    fs.readFile('./server-data/orders.json', 'utf-8', (err, data) => {
      if (!err) {
        const allOrders = JSON.parse(data);
        const isExistOrder = allOrders.some(ord => {
          return (ord && +ord.time === +order.time && +ord.date === +order.date && +ord.master_id === +order.master_id);
        });
        if (isExistOrder) {
          reject();
        } else {
          allOrders.push(order);
          fs.writeFile('./server-data/orders.json', JSON.stringify(allOrders), (err) => {
            if (!err) {
              resolve();
            }
            reject();
          });
        }
      } else {
        reject();
      }
    });
  });
}

function getOrders(masterId, date) {
  return new Promise((resolve, reject) => {
    fs.readFile('./server-data/orders.json', 'utf-8', (err, data) => {
      if (!err) {
        const allOrders = JSON.parse(data);
        const orders = allOrders.filter(ord => {
          return (ord && +ord.master_id === +masterId && +ord.date === +date);
        });
        resolve(orders);
      }
      reject();
    });
  });
}

function getOrdersStats() {
  const date = new Date();
  console.log(date.getDay());
  const currentUnixTime = Math.round(date.getTime() / 1000);
  const hoursUnix = date.getHours() * 3600;
  const minutesUnix = date.getMinutes() * 60;
  const dateStartUnix = currentUnixTime - hoursUnix - minutesUnix;
  const baseUnixTime = dateStartUnix + date.getTimezoneOffset() * 60;
  const startUnixTime = baseUnixTime - (date.getDay() + 6) * 24 * 3600;
  const endUnixTime = startUnixTime + 5 * 24 * 3600;
  return new Promise((resolve, reject) => {
    fs.readFile('./server-data/orders.json', 'utf-8', (err, data) => {
      if (err) {
        reject();
      }
      const allOrders = JSON.parse(data);
      const lastWeekOrders = allOrders.filter(order => {
        return order.date >= startUnixTime && order.date <= endUnixTime;
      });
      resolve(lastWeekOrders);
    });
  })

}

app.get('/', (req, res) => {
  res.send('Api is work!');
});



app.post('/users', (req, res) => {
  const existUser = users.find(us => us.email === req.body.email);
  if (existUser) {
    res.sendStatus(500);
  } else {
    const user = {
      id: Date.now(),
      email: req.body.email,
      password: req.body.password
    };
    users.push(user);
    res.send(user);
    saveUsers();
  }
});

app.post('/auth', (req, res) => {
  const user = req.body;
  const existUser = users.find(u => (u.email === user.email && u.password === user.password));
  if (existUser) {
    res.send(existUser.id.toString());
  } else {
    res.sendStatus(500);
  }
});

app.get('/users', (req, res) => {
  const id = req.query.id;
  const user = users.find(u => +u.id === +id);
  if (user) {
    res.send(JSON.stringify(user.email));
  } else {
    res.sendStatus(500);
  }
});

app.get('/master', (req, res) => {
  const serviceName = req.query.name;
  getWorks().then(works => {
    const findWork = JSON.parse(works).find(work => work.name === serviceName);
    if (findWork) {
      getMasters().then(masters => {
        const result = JSON.parse(masters).filter(m => findWork.masters.includes(m.id));
        if (result) {
          res.send(result);
        } else {
          res.sendStatus(500);
        }
      });
    } else {
      res.sendStatus(500);
    }
  });
});

app.get('/work', (req, res) => {
  const serviceName = req.query.name;
  getWorks().then(works => {
    const findWork = JSON.parse(works).find(work => work.name === serviceName);
    if (findWork) {
      res.send(findWork);
    } else {
      res.sendStatus(500);
    }
  });
});

app.get('/options', (req, res) => {
  const work_id = req.query.id;
  getOptions(work_id).then(result => {
    res.send(result);
  }).catch(_ => {
    res.sendStatus(500);
  });
});

app.post('/orders', (req, res) => {
  const order = req.body;
  saveOrder(order)
    .then(_ => res.send(true))
    .catch(_ => res.send(false));
});

app.get('/orders', (req, res) => {
  // TODO: check token
  const masterId = req.query.master_id;
  const date = req.query.date;
  if (masterId && date) {
    getOrders(masterId, date)
      .then(result => res.send(result))
      .catch(_ => res.sendStatus(500));
  }
});

app.get('/stats-orders', (req, res) => {
  getOrdersStats().then(data =>
    res.send(data)).catch(_ => res.sendStatus(500));
});


app.listen(3000, () => {
  console.log('Server started!');
  getUserList().then(data => users = JSON.parse(data));
});
